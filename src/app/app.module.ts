import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstComponent, FirstCallOutComponent } from './components/first/first.component';
import { SecondComponent, SecondCallOutComponent } from './components/second/second.component';
import { ThirdComponent, ThirdCallOutComponent } from './components/third/third.component';
import { FourthComponent, FourthCallOutComponent, BigTextComponent, BorderedComponent } from './components/fourth/fourth.component';
import { FifthComponent } from './components/fifth/fifth.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    FifthComponent,
    FirstCallOutComponent,
    SecondCallOutComponent,
    ThirdCallOutComponent,
    FourthCallOutComponent,
    BigTextComponent,
    BorderedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
