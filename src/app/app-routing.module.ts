import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent } from './components/first/first.component';
import { SecondComponent } from './components/second/second.component';
import { ThirdComponent } from './components/third/third.component';
import { FourthComponent } from './components/fourth/fourth.component';
import { FifthComponent } from './components/fifth/fifth.component';


const routes: Routes = [
  {path:  '1', component:  FirstComponent},
  {path:  '2', component:  SecondComponent},
  {path:  '3', component:  ThirdComponent},
  {path:  '4', component:  FourthComponent},
  {path:  '5', component:  FifthComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
