import { Component, OnInit } from '@angular/core';
import { of, BehaviorSubject } from 'rxjs';

type CalloutAction = 'dismiss' | 'accept';

@Component({
  selector: 'app-callout-third',
  template: `
  <ng-content select="[title]"></ng-content>
  <span>
    <ng-content select="[closePlaceHolder]"></ng-content>
  </span>
  <div>
    <ng-content select="[text]"></ng-content>
    <ng-content select="[actionButton]"></ng-content>
  </div>`,
  styles: ['div { height: 100px; width: 200px; text-align: center; border: 2px solid steelblue; display: flex; align-items: center;justify-content: center;}',
  'span {position: relative; display:block;top: 20px; left: 7px; cursor: pointer;}', '.warn {color: orange;}', '.danger {color: red}']
})
export class ThirdCallOutComponent {
}

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.scss']
})
export class ThirdComponent implements OnInit {
  callOutText$ = of('My callout warn text');
  callOutText2$ = of('My callout danger text');
  showCallOut$ = new BehaviorSubject(true);
  showCallOut2$ = new BehaviorSubject(true);
  title$ = new BehaviorSubject('My title');
  namedSlotExample = `
  <app-callout-third *ngIf="showCallOut2$ | async">
    <close-placeholder (click)="handleClose2()">x</close-placeholder>
    <title-slot></title><h3>{{title$ | async}}</h3></title-slot>
    <text-slot><div class="danger">{{callOutText2$ | async}}</div></text-slot>
    <action-slot><button (click)="handleCalloutAction('accept')">Accept</button></action-slot>
  </app-callout-third>
  `;

  constructor() { }

  ngOnInit(): void {
  }

  handleClose(): void {
    this.showCallOut$.next(false);
  }
  handleClose2(): void {
    this.showCallOut2$.next(false);
  }

  handleCalloutAction(action: CalloutAction): void {
    if (action === 'dismiss') {
      alert(action);
      this.handleClose();
    } else if (action === 'accept') {
      alert(action);
      this.handleClose2();
    } else {
      throw new Error(`type ${action} cannot be handled`);
    }
  }

}
