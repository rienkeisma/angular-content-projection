import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { of } from 'rxjs';

type CalloutAction = 'dismiss' | 'accept';

@Component({
  selector: 'app-callout-fourth',
  template: `
  <ng-content select="[title]"></ng-content>
  <span>
    <ng-content select="[closePlaceHolder]"></ng-content>
  </span>
  <div>
    <ng-content select="[text]"></ng-content>
    <ng-content select="[actionButton]"></ng-content>
  </div>`,
  styles: ['div { height: 100px; width: 200px; text-align: center; border: 2px solid steelblue; display: flex; align-items: center;justify-content: center;}',
  'span {position: relative; display:block;top: 20px; left: 7px; cursor: pointer;}', '.warn {color: orange;}', '.danger {color: red}']
})
export class FourthCallOutComponent {
}

@Component({
  selector: 'app-big-text',
  template: `
  <ng-content></ng-content>
  `,
  styles: [':host { font-size: 30px; }']
})
export class BigTextComponent {
}

@Component({
  selector: 'app-bordered',
  template: `
  <ng-content></ng-content>
  `,
  styles: [':host { border: 1px solid steelblue; display: block;}']
})
export class BorderedComponent {
}

@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.scss']
})
export class FourthComponent implements OnInit {
  callOutText$ = of('My callout warn text');
  callOutText2$ = of('My callout danger text');
  showCallOut$ = new BehaviorSubject(true);
  showCallOut2$ = new BehaviorSubject(true);
  title$ = new BehaviorSubject('My title');

  constructor() { }

  ngOnInit(): void {
  }
  handleClose(): void {
    this.showCallOut$.next(false);
  }
  handleClose2(): void {
    this.showCallOut2$.next(false);
  }

  handleCalloutAction(action: CalloutAction): void {
    if (action === 'dismiss') {
      alert(action);
      this.handleClose();
    } else if (action === 'accept') {
      alert(action);
      this.handleClose2();
    } else {
      throw new Error(`type ${action} cannot be handled`);
    }
  }

}
