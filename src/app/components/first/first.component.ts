import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-callout-first',
  template: `
    <span (click)="close()">x</span>
    <div>{{text$ | async}}</div>
  `,
  styles: ['div { height: 100px; width: 200px; text-align: center; border: 2px solid steelblue; display: flex; align-items: center;justify-content: center;}',
  'span {position: relative; display:block;top: 20px; left: 7px; cursor: pointer;}']
})
export class FirstCallOutComponent implements OnInit {
  @Input() text$: Observable<string>;
  @Output() closeCallout = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }
  close(): void {
    this.closeCallout.emit(true);
  }
}

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {
  callOutText$ = of('My callout text');
  showCallOut$ = new BehaviorSubject(true);

  constructor() { }

  ngOnInit(): void {
  }

  handleClose(): void {
    this.showCallOut$.next(false);
  }

}
