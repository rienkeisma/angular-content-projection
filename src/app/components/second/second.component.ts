import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';

type CalloutAction = 'dismiss' | 'accept';

@Component({
  selector: 'app-callout-second',
  template: `
  <h3 *ngIf="title$ | async; let title">{{title}}</h3>
  <span (click)="close()">x</span>
  <div [ngClass]="{'warn': color === 'warn', 'danger': color === 'danger'}">
    {{text$ | async}}
    <button *ngIf="showAccept" (click)="doAccept()">Accept</button>
    <button *ngIf="showDismiss" (click)="doDismiss()">Dismiss</button>
  </div>`,
  styles: ['div { height: 100px; width: 200px; text-align: center; border: 2px solid steelblue; display: flex; align-items: center;justify-content: center;}',
  'span {position: relative; display:block;top: 20px; left: 7px; cursor: pointer;}', '.warn {color: orange;}', '.danger {color: red}']
})
export class SecondCallOutComponent implements OnInit {
  @Input() text$: Observable<string>;
  @Input() title$: Observable<string | boolean> = of(false);
  @Input() color: string;
  @Input() showAccept: boolean;
  @Input() showDismiss: boolean;
  @Output() closeCallout = new EventEmitter<boolean>();
  @Output() accept = new EventEmitter<void>();
  @Output() dismiss = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }
  close(): void {
    this.closeCallout.emit(true);
  }
  doAccept(): void {
    this.accept.emit();
  }
  doDismiss(): void {
    this.dismiss.emit();
  }
}

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  callOutText$ = of('My callout warn text');
  callOutText2$ = of('My callout danger text');
  showCallOut$ = new BehaviorSubject(true);
  showCallOut2$ = new BehaviorSubject(true);
  title$ = new BehaviorSubject('My title');

  constructor() { }

  ngOnInit(): void {
  }

  handleClose(): void {
    this.showCallOut$.next(false);
  }
  handleClose2(): void {
    this.showCallOut2$.next(false);
  }

  handleCalloutAction(action: CalloutAction): void {
    if (action === 'dismiss') {
      alert(action);
      this.handleClose();
    } else if (action === 'accept') {
      alert(action);
      this.handleClose2();
    } else {
      throw new Error(`type ${action} cannot be handled`);
    }
  }
}
